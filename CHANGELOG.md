# Version 0.0.8
* Correct a bug with display of textareas.
* Set bordered field as the default field and define filed-clean.

# Version 0.0.7
* Create general field style (not just aplicable to inputs).

# Version 0.0.6
* Create table default style.
* Create image styles.

# Version 0.0.5
* Correct a bug with hover effect of buttons in bars.

# Version 0.0.4
* Add default footer style.
* Add progressbar styles.
* Add gradients.

# Version 0.0.3
* Add default statement to a lot of variables.
* Add notifications styles.